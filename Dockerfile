FROM node:onbuild AS builder
RUN REACT_APP_HOST="viewer.mbi9tccu6y.us-east-2.elasticbeanstalk.com" npm run build

FROM nginx:latest
COPY --from=builder /usr/src/app/build /usr/share/nginx/html