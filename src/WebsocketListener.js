import EventBus from 'eventbusjs'
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {getHost} from './Config.js'

/*
  A component which listens to WS topics and sends app-specific events.
*/
class WebsocketListener {

  /*
    Start listening.
  */
  start() {
    var socket = new SockJS(getHost() + '/buildviewer-ws');
    this.stompClient = Stomp.over(socket);
    var client = this.stompClient;
    client.connect({}, function (frame) {
        client.subscribe('/topic/host', function (event) {
            if (event.body === 'refresh') {
              EventBus.dispatch("refresh-hosts");
            }
        });
        client.subscribe('/topic/build', function (event) {
            if (event.body === 'refresh') {
              EventBus.dispatch("refresh-builds");
            }
        });
    });
  }

  /*
    Stop listening and free all resources.
  */
  stop() {
    this.stompClient.disconnect();
  }
}

export default WebsocketListener