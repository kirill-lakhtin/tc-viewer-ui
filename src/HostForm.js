import React, { Component } from 'react';
import { addHost } from './Requests.js';
/**
  A component that represents a 'add a host' form.
*/
class HostForm extends Component {

  constructor() {
    super();
    this.state = {url: ''};
  }

  render() {
    this.createNewHost = function(e) {
      addHost(this.state.url);
      this.setState({url: ''});
    }

    this.onChange = function(e) {
      this.setState({
        url: e.target.value
      });
    }
    return <form onSubmit={e => this.createNewHost(e)}>
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Enter a new Teamcity host" value={this.state.url} onChange={e => this.onChange(e)}/>
              <div className="input-group-append">
                <button className="btn btn-outline-secondary" type="button" onClick={e => this.createNewHost(e)}>Add</button>
              </div>
            </div>
          </form>
  }

}
export default HostForm;
