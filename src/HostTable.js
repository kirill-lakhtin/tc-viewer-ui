import React, { Component } from 'react';
import RefreshableTable from './RefreshableTable.js'
import {loadHosts, deleteHost} from "./Requests.js"

/*
  A component that represent a table with hosts.
*/
class HostTable extends Component {

  render() {
      this.onClick = function(e, row) {
        const id = row.original.id;
        //propagate an event to refresh tables
        deleteHost(id);
      }

      const columns = [{
        Header: 'Host',
        accessor: 'url'
      },
      {
        Header: 'Actions',
        Cell: row => (<input type="button" value="Delete" className="btn btn-danger mx-auto" onClick={(e) => this.onClick(e, row)}/>)
      }]

      return <RefreshableTable
        loadPromise = {loadHosts}
        defaultPageSize={5}
        columns={columns}
        eventName="refresh-hosts"
      />
    }
}
export default HostTable;