import React, { Component } from 'react';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import EventBus from 'eventbusjs'

/**
  An abstract table which is refreshed if 'refresh' event has been fired.
*/
class RefreshableTable extends Component {

  constructor() {
    super();
    this.state = {
          data: [],
          pages: null,
          loading: true
        };
  }

  /*
    Subscribe to events.
  */
  componentDidMount() {
    if (this.props.eventName) {
      var those = this;
      EventBus.addEventListener(this.props.eventName, function() {
        those.fetchData({pageSize: those.state.pageSize, page: 0, sorted: those.state.sorted});
      })
    }
  }

  /*
    Unsubscribe from events.
  */
  componentWillUnmount() {
    if (this.props.eventName) {
      EventBus.removeEventListener(this.props.eventName);
     }
  }

  /**
    Converts table sorting data to more convenient form.
  */
  extractSort = function(sort) {
    var sorted = null;
    if (sort && sort.length > 0) {
      sorted = {
        field: sort[0].id,
        desc: sort[0].desc
      }
    }
    return sorted;
  }

  /**
    Loads data from the server.
  */
  fetchData = function(state, instance) {
    // store current table settings for external reloading
    this.setState({
      pageSize: state.pageSize,
      page: state.page,
      sorted: state.sorted,
      loading: true
    });

    var those = this;
    this.props.loadPromise(state.pageSize, state.page, this.extractSort(state.sorted))
    .then(response => response.json())
    .then(data => {
      those.setState({
        data: data.elements,
        pages: Math.ceil(data.totalCount / state.pageSize),
        loading: false
      })
    })
  }

  render() {
    const { data, pages, loading } = this.state;
    return <ReactTable
                manual
                data={data}
                pages={pages}
                loading={loading}
                onFetchData={this.fetchData.bind(this)}
                defaultPageSize={this.props.defaultPageSize}
                columns={this.props.columns}
              />

  }
}
export default RefreshableTable;