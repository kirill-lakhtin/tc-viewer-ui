/**
  Determines the backend host.
*/
export function getHost() {
  if (process.env.REACT_APP_HOST) {
    return "http://" + process.env.REACT_APP_HOST;
  } else {
    return "http://localhost:8080";
  }
}
