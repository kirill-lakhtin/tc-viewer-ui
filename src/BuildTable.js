import React, { Component } from 'react';
import RefreshableTable from './RefreshableTable.js'
import {loadBuilds} from "./Requests.js"

/*
  A component that represent a table with builds.
*/
class BuildTable extends Component {

  render() {
      const columns = [{
        Header: 'Host',
        accessor: 'host'
      },
      {
        Header: 'Start Date',
        id: 'startDate',
        accessor: date => {
          //convert date from mills to human-readable string
          return new Date(date.startDate).toLocaleString();
        }
      },
      {
        Header: 'Build type id',
        accessor: 'buildTypeId'
      }]

      return <RefreshableTable
        loadPromise = {loadBuilds}
        defaultPageSize={10}
        columns={columns}
        eventName="refresh-builds"
      />
    }
}

export default BuildTable;