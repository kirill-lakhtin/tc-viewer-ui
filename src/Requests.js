import {getHost} from './Config.js'

/**
  A function to retrieve hosts from the server.
*/
export function loadHosts(pageSize, page, sorted) {
    return load(getHost()+ "/host", pageSize, page, sorted);
}

/**
  A function to retrieve builds from the server.
*/
export function loadBuilds(pageSize, page, sorted) {
   return load(getHost() + "/build", pageSize, page, sorted);
}

/**
  A function for creating a new host.
*/
export function addHost(url) {
  return fetch(getHost() + "/host", {
         method: 'POST',
         body: JSON.stringify({'url': url}),
         headers: {
             'Accept': 'application/json, text/plain, */*',
             'Content-Type': 'application/json'
           }});
}

/**
  A function for deleting a host.
*/
export function deleteHost(id) {
  return fetch(getHost() + "/host/"+id, {method: 'delete'});
}

function load(url, pageSize, page, sorted) {
  var url = url + "?pageSize="+pageSize+"&page="+page;
  if (sorted) {
    url = url + "&sortField=" + sorted.field + "&descSort=" + sorted.desc;
  }
  return fetch(url, {cache: "no-store"});
}