import React, { Component } from 'react';
import HostTable from './HostTable.js';
import BuildTable from './BuildTable.js';
import HostForm from './HostForm.js';
import WebsocketListener from './WebsocketListener.js';

/**
  The base component.
*/
class App extends Component {

  constructor() {
    super();
    this.websocketListener = new WebsocketListener();
  }

  componentDidMount() {
    this.websocketListener.start();
  }

  componentWillUnmount() {
    this.websocketListener.stop();
  }

  render() {
    return <div className="container">
                <h1 className="text-center px-3 py-3 pt-md-5 pb-md-4 mx-auto">Builds viewer</h1>
                <h2>Hosts</h2>
                <HostForm/>
                <HostTable/>
                <h2>Builds</h2>
                <BuildTable/>
          </div>
  }
}

export default App;
